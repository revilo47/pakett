\name{signsshort2}
\alias{signsshort2}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{signsshort2
%%  To add the symbol '<'
}
\description{
%%  When called, adds the symbol '<' to the console.
}
\usage{
signsshort2(x)
}
%- Ment to be used as an addin as a workaround to add shortcuts to R in windows for keyboards that don't have these signs next to z.
\arguments{
  \item{x}{
%%     Takes no arguments.
}
}
\details{
%%  No further details to add.
}
\value{
%%  Returns a list of 3, with $ranges, $text and $id.

}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  Oliver Bollverk (R specialist at Statistics Estonia)
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (x)
{
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line

