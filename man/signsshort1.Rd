
\name{signsshort1}
\alias{signsshort1}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{signsshort1
}
\description{
Returns the symbol '>' to console
}
\usage{
signsshort1()
}
%- Ment to be used as an addin as a workaround to add shortcuts to R in windows for keyboards that don't have these signs next to z.
\arguments{
  \item{Takes no arguments}{
%%     Takes no arguments.
}
}
\details{
}
\value{
%%  Returns a list of 3, with $ranges, $text and $id.

}
\references{
%% put references to the literature/web site here
}
\author{
Oliver Bollverk (R specialist at Statistics Estonia)
}
\note{
%% further notes
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
## signsshort1()
## The function is currently defined as
signsshort1 <- function() {
  rstudioapi::insertText(">")
}
}

% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
